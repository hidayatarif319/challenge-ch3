const express = require("express");
const app = express();
const port = 8000;
const path = require("path");
let userData = require("./user-data.json");
const userController = require('./controllers/userController');

app.set(`view engine`, `ejs`);
app.use(express.urlencoded({ extended: false }));
app.use(express.static(`public`));
app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
app.use(express.json());

// POST /articles, buat artikel baru
app.post("/done", (req, res) => {
  user_game.create({
      email: req.body.email,
      password: req.body.password,
    })
    .then(user_game => {
      res.send("User berhasil dibuat");
    });
});

// GET /articles/create, menampilkan form create user_game
app.get("/createUG", (req, res) => {
  res.status(200);
  res.render("createUserGame");
});

// GET /articles/create, menampilkan form create user_game
app.get("/createUGB", (req, res) => {
  res.status(200);
  res.render("createUserGameBio");
});

// GET /articles/create, menampilkan form create user_game
app.get("/createUGH", (req, res) => {
  res.status(200);
  res.render("createUserGameHistory");
});

// 404 Handler
app.use(function (err, req, res, next) {
  res.status(404);
  res.json({
    status: "Fail",
    errors: "Are you lost?",
  });
});

app.get(`/`, (req, res) => {
  res.status(200);
  res.render(`index`);
});

app.get(`/games`, (req, res) => {
  res.status(200);
  res.render(`games`);
});

app.get(`/login`, (req, res) => {
  let inputEmailAddress = req.body.inputEmailAddress;
  let inputPassword = req.body.inputPassword;
  if (inputEmailAddress == "admin@admin.com" && inputPassword == "Admin") {
    res.redirect(`/dashboard`);
  }
  res.status(200);
  res.render(`login`);
});

// READ /dashboard, menampilkan semua data user_game
app.get("/dashboardUG", (req, res) => {
  user_game.findAll().then((user_game) => {
    res.render("dashboardUserGame", {
      user_game,
    });
  });
});

// READ /dashboard, menampilkan semua data user_game_biodata
app.get("/dashboardUGB", (req, res) => {
  user_game_biodata.findAll().then((user_game_biodata) => {
    res.render("dashboardUserGameBio", {
      user_game_biodata,
    });
  });
});

// READ /dashboard, menampilkan semua data user_game_history
app.get("/dashboardUGH", (req, res) => {
  user_game_history.findAll().then((user_game_history) => {
    res.render("dashboardUserGameHistory", {
      user_game_history,
    });
  });
});

app.get(`/data-user`, (req, res) => {
  res.status(200).json(userData);
});