    let playerRock = document.getElementById('rock');
    let playerPaper = document.getElementById('paper');
    let playerScissor = document.getElementById('scissor');
    let comRock = document.getElementById('cpu-rock');
    let comPaper = document.getElementById('cpu-paper');
    let comScissor = document.getElementById('cpu-scissor');
    let refreshButton = document.getElementById('refresh');
    let operator = document.getElementById('operator');
    const SELECTIONS = ['rock', 'paper', 'scissor'];

    // Reset Selected Function
    function resetSelected() {
      playerRock.classList.remove('selected');
      playerPaper.classList.remove('selected');
      playerScissor.classList.remove('selected');
    }

    function resetSelectedCOM() {
        comRock.classList.remove('selected');
        comPaper.classList.remove('selected');
        comScissor.classList.remove('selected');
      }

    // Reset Game Function
    function refreshGame(){
      location.reload(true);
    }

    // Computer Function
    function getComHand (){
      const randomIndex = Math.floor(Math.random() * SELECTIONS.length);
      return SELECTIONS[randomIndex];
    }

    // Match Function
    function matchRPS(hand1, hand2) {
            //Isi disini
            if (hand1 == "paper" && hand2 == "rock") {
            return "Player 1 Win"; //Yang menang hand1
            } else if (hand1 == "rock" && hand2 == "scissor") {
            return "Player 1 Win"; //Yang menang hand1
            } else if (hand1 == "scissor" && hand2 == "paper") {
            return "Player 1 Win"; //Yang menang hand1
            } else if (hand1 == "rock" && hand2 == "paper") {
            return "COM Win"; //Yang menang hand1
            } else if (hand1 == "paper" && hand2 == "scissor") {
            return "COM Win"; //Yang menang hand1
            } else if (hand1 == "scissor" && hand2 == "rock") {
            return "COM Win"; //Yang menang hand1
            } else if (hand1 == "paper" && hand2 == "paper") {
            return "DRAW"; //Yang menang hand1
            } else if (hand1 == "rock" && hand2 == "rock") {
            return "DRAW"; //Yang menang hand1
            } else if (hand1 == "scissor" && hand2 == "scissor") {
            return "DRAW"; //Yang menang hand1
            }
            //Output: yg menang siapa, bisa "hand1", "hand2", "draw"
        }

    // Player Rock Function
    playerRock.addEventListener("click", function(e){
      resetSelected();
      playerRock.classList.add('selected');
      let hand1 = playerRock.id;
      // Generate Random Com
      let hand2 = getComHand();
      if (hand2 == 'rock'){
        resetSelectedCOM();
        comRock.classList.add('selected');
      } else if (hand2 == 'paper'){
        resetSelectedCOM();
        comPaper.classList.add('selected');
      } else {
        resetSelectedCOM();
        comScissor.classList.add('selected');
      }
        // Match kan
      var result = matchRPS (hand1,hand2)
        // Cetak result
      operator.classList.remove('operator-default')
        if(result == 'Player 1 Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'COM Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'DRAW'){
            operator.classList.remove('player-win');
            operator.innerText = result;
            operator.classList.add('player-draw');
        }
    })

    // Player Paper Function
    playerPaper.addEventListener("click", function(e){
        resetSelected();
        playerPaper.classList.add('selected');
        let hand1 = playerPaper.id;
        // Generate Random Com
        let hand2 = getComHand();
        if (hand2 == 'rock'){
            resetSelectedCOM();
            comRock.classList.add('selected');
          } else if (hand2 == 'paper'){
            resetSelectedCOM();
            comPaper.classList.add('selected');
          } else {
            resetSelectedCOM();
            comScissor.classList.add('selected');
          }
          // Match kan
        let result = matchRPS(hand1,hand2);
        // Cetak result
        operator.classList.remove('operator-default')
        if(result == 'Player 1 Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'COM Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'DRAW'){
            operator.classList.remove('player-win');
            operator.innerText = result;
            operator.classList.add('player-draw');
        }
      })

    // Player Scissor Function
    playerScissor.addEventListener("click", function(e){
        resetSelected();
        playerScissor.classList.add('selected');
        let hand1 = playerScissor.id;
        // Generate Random Com
        let hand2 = getComHand();
        if (hand2 == 'rock'){
            resetSelectedCOM();
            comRock.classList.add('selected');
          } else if (hand2 == 'paper'){
            resetSelectedCOM();
            comPaper.classList.add('selected');
          } else {
            resetSelectedCOM();
            comScissor.classList.add('selected');
          }
          // Match kan
        let result = matchRPS(hand1,hand2);
        // Cetak result
        operator.classList.remove('operator-default')
        if(result == 'Player 1 Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'COM Win'){
            operator.classList.remove('player-draw');
            operator.innerText = result;
            operator.classList.add('player-win');
        } else
        if(result == 'DRAW'){
            operator.classList.remove('player-win');
            operator.innerText = result;
            operator.classList.add('player-draw');
        }
      })

    refreshButton.addEventListener("click", function(e){
      refreshGame();
    })